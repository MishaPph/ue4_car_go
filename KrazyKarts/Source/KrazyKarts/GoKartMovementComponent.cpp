// Fill out your copyright notice in the Description page of Project Settings.


#include "GoKartMovementComponent.h"

// Sets default values for this component's properties
UGoKartMovementComponent::UGoKartMovementComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGoKartMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

// Called every frame
void UGoKartMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
	if (GetOwnerRole() == ROLE_AutonomousProxy || GetOwner()->GetRemoteRole() == ROLE_SimulatedProxy)
	{
		LastMove = CreateMove(DeltaTime);
		SimulateMove(LastMove);
	}
}

FGoKartMove UGoKartMovementComponent::CreateMove(float DeltaTime)
{
	FGoKartMove Move;
	Move.DeltaTime = DeltaTime;
	Move.Throttles = Throttles;
	Move.SteeringThrow = SteeringThrow;
	Move.Time = GetWorld()->TimeSeconds;
	return Move;
}

FGoKartMove UGoKartMovementComponent::GetLastMove()
{
	return LastMove;
}

void UGoKartMovementComponent::SimulateMove(const FGoKartMove& Move)
{
	FVector Force = GetOwner()->GetActorForwardVector() * MaxDrivingForce * Move.Throttles;

	Force += GetAirResistance();
	Force += GetRollingResistance();
	
	const FVector Acceleration = Force/Mass;

	Velocity = Velocity + Acceleration * Move.DeltaTime;

	ApplyRotation(Move.DeltaTime, Move.SteeringThrow);
	
	UpdateLocationFromVelocity(Move.DeltaTime);
}

void UGoKartMovementComponent::SetVelocity(const FVector& Vector)
{
	Velocity = Vector;
}

FVector UGoKartMovementComponent::GetVelocity() const
{
	return Velocity;
}

void UGoKartMovementComponent::SetThrottles(float Value)
{
	Throttles = Value;
}

void UGoKartMovementComponent::SetSteeringThrow(float Value)
{
	SteeringThrow = Value;
}

FVector UGoKartMovementComponent::GetAirResistance()
{
	float SpeedSquared = Velocity.SizeSquared();
	return -Velocity.GetSafeNormal() * SpeedSquared * DragCoefficient;
}

FVector UGoKartMovementComponent::GetRollingResistance()
{
	float AccelerationDueToGravity = GetWorld()->GetGravityZ()/100;
	float NormalForce = Mass * AccelerationDueToGravity;
	return Velocity.GetSafeNormal() * RollingResistanceCoefficient * NormalForce;
}

void UGoKartMovementComponent::ApplyRotation(float DeltaTime, float SteringThrow)
{
	float DeltaLocation = FVector::DotProduct(GetOwner()->GetActorForwardVector(),  Velocity) * DeltaTime;
	float RotationAngle = DeltaLocation  / MinTurningRadius * SteringThrow;
	FQuat RotationDelta(GetOwner()->GetActorUpVector(), RotationAngle);

	Velocity = RotationDelta.RotateVector(Velocity);
	
	GetOwner()->AddActorWorldRotation(RotationDelta, true);
}

void UGoKartMovementComponent::UpdateLocationFromVelocity(float DeltaTime)
{
	const FVector Traslation = Velocity * DeltaTime * 100;
	
	FHitResult Hit;
	GetOwner()->AddActorWorldOffset(Traslation, true, &Hit);
	if(Hit.IsValidBlockingHit())
	{
		Velocity = FVector::ZeroVector;
	}
}

