// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GoKartMovementComponent.h"
#include "GoKartMovementReplicator.generated.h"


USTRUCT()
struct FGoKartState
{
	GENERATED_USTRUCT_BODY()

    UPROPERTY()
	FTransform Transform;
	
	UPROPERTY()
	FVector Velocity;

	UPROPERTY()
	FGoKartMove LastMove;
};

struct FHermiteCubicSpline
{
	FVector TargetLocation;
	FVector StartLocation;
	FVector StartDerivative;
	FVector TargetDerivative;

	FVector InterpolateLocation(float LerpRation) const
	{
		return FMath::CubicInterp(StartLocation, StartDerivative, TargetLocation, TargetDerivative, LerpRation);
	}
	FVector InterpolateDerivative(float LerpRation) const
	{
		return FMath::CubicInterpDerivative(StartLocation, StartDerivative, TargetLocation, TargetDerivative, LerpRation);
	}
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class KRAZYKARTS_API UGoKartMovementReplicator : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGoKartMovementReplicator();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	void ClientTick(float DeltaTime);

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	float VelocityToDerivative();
	FHermiteCubicSpline CreateSpline();
	void InterpolateLocation(float Lerp, const FHermiteCubicSpline& Spline);
	void InterpolateVelocity(float Lerp, const FHermiteCubicSpline& Spline);
	void InterpolateRotation(float Lerp);

private:
	
	void ClearAcknowledgeMoves(FGoKartMove LastMove);

	void UpdateServerState(const FGoKartMove& Move);
	void SimulatedProxy_OnRepServerState();
	void AutonomousProxy_OnRepServerState();

	UFUNCTION()
    void OnRep_ServerState();

	UFUNCTION(Server, Reliable, WithValidation)
    void Server_SendMove(FGoKartMove Move);

	UPROPERTY(ReplicatedUsing = OnRep_ServerState)
	FGoKartState ServerState;

	TArray<FGoKartMove> UnacknowledgedMoves;
	
	UPROPERTY(VisibleAnywhere)
	UGoKartMovementComponent* MovementComponent;

    UPROPERTY(VisibleAnywhere)
    USceneComponent* MeshOffsetRoot;

	UFUNCTION(BlueprintCallable)
	void SetMeshOffsetRoot(USceneComponent* Root){ MeshOffsetRoot = Root;}

	FTransform ClientStartTransform;
	FVector ClientStartVelocity;

	float ClientTimeSinceUpdate;
	float ClientTimeBetweenLastUpdates;

	float ClientSimulatedTime;
};
